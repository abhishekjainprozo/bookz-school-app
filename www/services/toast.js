(function() {
  "use strict";
  angular.module("BookzSchoolApp").service("toast", toast);
  toast.$inject = ["$ionicPopup", "$timeout"];

  function toast($ionicPopup, $timeout) {
    var that = this;
    var duration = {
      short: 1000,
      medium: 2000,
      long: 3000
    };

    that.show = show;

    function show(msg, time) {
      if (window.plugins && window.plugins.toast) {
        window.plugins.toast.showWithOptions({
          message: msg,
          duration: duration[time.toLowerCase()],
          position: "center"
        });
      } else {
        var toast = $ionicPopup.show({
          cssClass: "pz-reset-password-toast",
          template: msg
        });
        $timeout(function() {
          toast.close();
        }, duration[time.toLowerCase()]);
      }
    }
  }
})();
