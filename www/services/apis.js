(function() {
  "use strict";
  angular.module("BookzSchoolApp").service("apis", apis);
  apis.$inject = ["$http", "constants"];

  function apis($http, constants) {
    var that = this; // jshint ignore:line

    that.schoolCode = schoolCode;
    that.resendOtp = resendOtp;
    that.requestOTP = requestOTP;
    that.verifyOTP = verifyOTP;
    that.packages = packages;
    that.states = states;
    that.cities = cities;
    that.placeRequest = placeRequest;
    that.reqDetails = reqDetails;
    that.cancelOrder = cancelOrder;
    that.getAppVersion = getAppVersion;

    that.RazorpayCapture = RazorpayCapture;
    that.RazorpayTransfer = RazorpayTransfer;
    that.RazorpayRefund = RazorpayRefund;

    that.sendSms = sendSms;

    /////

    function schoolCode(code) {
      return $http.get(constants.url + "code?code=" + code);
    }

    function resendOtp(mobile) {
      return $http.post(constants.url + "resend_otp?mobile=" + mobile);
    }

    function requestOTP(first_name, last_name, mobile, email, class_number) {
      return $http.post(constants.url + "otp?mobile=" + mobile, {
        first_name: first_name,
        last_name: last_name,
        mobile: mobile,
        email: email,
        class_number: class_number
      });
    }

    function verifyOTP(mobile, otp, studentID, code) {
      return $http.post(
        constants.url +
          "verify_otp?mobile=" +
          mobile +
          "&otp=" +
          otp +
          "&student=" +
          studentID +
          "&code=" +
          code
      );
    }

    function packages(code, studentID) {
      return $http.get(
        constants.url + "products?code=" + code + "&student=" + studentID
      );
    }

    function states() {
      return $http.get(constants.url + "states");
    }

    function cities(stateID) {
      return $http.get(constants.url + "city?state=" + stateID);
    }

    function placeRequest(params) {
      return $http.post(constants.url + "order", params);
    }

    function reqDetails(reqNb, mobNb) {
      if (mobNb)
        return $http.get(
          constants.url + "order?tracking_number=" + reqNb + "&mobile=" + mobNb
        );
      else return $http.get(constants.url + "order?tracking_number=" + reqNb);
    }

    function cancelOrder(reqNb) {
      return $http.delete(constants.url + "order?id=" + reqNb);
    }

    function getAppVersion(currVer) {
      return $http.get(constants.url + "version?version=" + currVer);
    }

    function RazorpayCapture(payID, amount) {
      var key = constants.razorpayKey;
      var secret = constants.razorpaySecret;
      var auth = btoa(key + ":" + secret);

      var url = "https://api.razorpay.com/v1/payments/" + payID + "/capture";
      var params = {
        amount: amount
      };

      return $http({
        url: url,
        method: "POST",
        headers: {
          Authorization: "Basic " + auth
        },
        data: params
      });
    }

    function RazorpayTransfer(payID, accountID, amount) {
      var key = constants.razorpayKey;
      var secret = constants.razorpaySecret;
      var auth = btoa(key + ":" + secret);

      var url = "https://api.razorpay.com/v1/payments/" + payID + "/transfers";
      var params = {
        transfers: [
          {
            account: accountID,
            amount: amount,
            currency: "INR"
          }
        ]
      };

      return $http({
        url: url,
        method: "POST",
        headers: {
          Authorization: "Basic " + auth
        },
        data: params
      });
    }

    function RazorpayRefund(payID) {
      var key = constants.razorpayKey;
      var secret = constants.razorpaySecret;
      var auth = btoa(key + ":" + secret);

      var url = "https://api.razorpay.com/v1/payments/" + payID + "/refund";

      return $http({
        url: url,
        method: "POST",
        headers: {
          Authorization: "Basic " + auth
        }
      });
    }

    function sendSms(config) {
      var apiKey = "kpxkPpkLHe8-cSrVG3ke6bMNkZ94xG2cT0euk3FMwy";
      var sender = "PROZZO";
      var numbers = [919871488896, 919205242365].join(",");
      // var numbers = [919871488896, 918700147078].join(",");
      var message;
      if (config.type === "capture_fail") {
        message =
          "Error while capturing Razorpay payment for payment Id " +
          config.paymentId +
          " made by " +
          config.buyerName +
          " (" +
          config.buyerContact +
          ")";
      } else if (config.type === "order_fail") {
        message =
          "Error while placing order for " +
          config.buyerName +
          " (" +
          config.buyerContact +
          ") for school code " +
          config.schoolCode;
      }

      var params =
        "?apiKey=" +
        apiKey +
        "&sender=" +
        sender +
        "&numbers=" +
        numbers +
        "&message=" +
        message;
      var endpoint = "https://api.textlocal.in/send/";
      var url = endpoint + params;

      return $http.post(url, params);
    }

    //
  }
})();
