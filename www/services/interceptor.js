(function() {
  "use strict";
  angular.module("BookzSchoolApp").service("interceptor", interceptor);
  interceptor.$inject = ["$q"];

  function interceptor($q) {
    var that = this;

    that.request = function(conf) {
      showLoader();
      conf.timeout = 30000;
      return conf;
    };
    that.requestError = function(rejection) {
      hideLoader();
      return $q.reject(rejection);
    };
    that.response = function(response) {
      hideLoader();
      return response;
    };
    that.responseError = function(rejection) {
      hideLoader();
      return $q.reject(rejection);
    };
  }

  ///

  function showLoader() {
    document.getElementById("pz-loader").style.display = "block";
  }

  function hideLoader() {
    document.getElementById("pz-loader").style.display = "none";
  }
})();
