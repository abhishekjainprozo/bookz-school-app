(function() {
  "use strict";
  angular.module("BookzSchoolApp").service("l10n", l10n);
  l10n.$inject = ["$http", "constants", "$rootScope"];

  function l10n($http, constants, $rootScope) {
    var that = this; // jshint ignore:line
    that.set = set;
    that.get = get;

    var strings = {};

    function set() {
      var url = "xxx";
      $http
        .get(url)
        .then(function(res) {})
        .catch(function(err) {
          var temp = [
            { key: "__thisLang", value: "English" },
            {
              key: "schoolCode__1",
              value: "Please fill in your six digits School Code"
            },
            { key: "schoolCode__2", value: "Don't know the code? Click here." },
            { key: "schoolCode__3", value: "eg 657832" },
            { key: "schoolCode__4", value: "Track old order" },

            { key: "studentInfo__1", value: "student's name" },
            { key: "studentInfo__2", value: "First name" },
            { key: "studentInfo__3", value: "Last name" },
            { key: "studentInfo__4", value: "E-mail ID" },
            { key: "studentInfo__5", value: "example@abc.com" },
            { key: "studentInfo__6", value: "Class" },
            { key: "studentInfo__7", value: "Mobile Number" },
            { key: "studentInfo__8", value: "Your mobile number" },
            { key: "studentInfo__9", value: "Request OTP" },

            { key: "footer__1", value: "Selling partner" },

            { key: "otpVerify__1", value: "Enter OTP sent on " },
            { key: "otpVerify__2", value: "Verify OTP" },
            { key: "otpVerify__3", value: "Didn't get an OTP?" },
            { key: "otpVerify__4", value: "Resend" },
            { key: "otpVerify__5", value: "eg 123456" },

            { key: "loginsuccessful__1", value: "Login Successful!" },
            { key: "loginsuccessful__2", value: "Start Purchase" },

            { key: "packages__1", value: "xxx Package" },
            { key: "packages__2", value: "Total amount" },
            { key: "packages__3", value: "Place Request" },
            { key: "packages__4", value: "GST applicable" },

            { key: "address__1", value: "xxx Package" },
            { key: "address__2", value: "Total" },
            {
              key: "address__3",
              value: "Please fill in your delivery address"
            },
            {
              key: "address__4",
              value: "Your bundle will be delivered to this address"
            },
            { key: "address__5", value: "Name" },
            { key: "address__6", value: "Billing Address" },
            { key: "address__7", value: "Shipping Address" },
            { key: "address__8", value: "Add Billing Address" },
            { key: "address__9", value: "Mobile Number" },
            { key: "address__10", value: "Place Request" },

            { key: "address__11", value: "First Name" },
            { key: "address__12", value: "Last Name" },
            { key: "address__13", value: "Address" },
            { key: "address__15", value: "Landmark" },
            { key: "address__16", value: "Pincode" },
            { key: "address__17", value: "State" },
            { key: "address__18", value: "City" },
            { key: "address__19", value: "Your mobile number" },
            { key: "address__20", value: "Place Request" }
          ];
          for (var i = 0; i < temp.length; i++) {
            strings[temp[i].key] = temp[i].value;
          }
        });
    }

    function get(keyName, replacement) {
      var value = (strings && strings[keyName]) || keyName;
      if (replacement) value = value.replace("xxx", replacement);
      return value;
    }
  }
})();
