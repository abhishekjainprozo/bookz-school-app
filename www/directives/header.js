(function() {
  "use strict";
  angular.module("BookzSchoolApp").directive("pzHeader", pzHeader);
  pzHeader.$inject = [];

  function pzHeader() {
    return {
      restrict: "E",
      scope: {
        logo: "@",
        name: "@",
        address: "@"
      },
      controller: [
        "$scope",
        "$rootScope",
        function($scope, $rootScope) {
          if (!$scope.logo && $rootScope.codeData) $scope.logo = $rootScope.codeData.school.logo_link;
          if (!$scope.name && $rootScope.codeData) $scope.name = $rootScope.codeData.school.name;
          if (!$scope.address && $rootScope.codeData) $scope.address = $rootScope.codeData.school.address;
        }
      ],
      link: function(scope, elem, attrs) {
        if (!scope.logo) scope.logo = attrs.logo;
        if (!scope.name) scope.name = attrs.name;
        if (!scope.address) scope.address = attrs.address;
      },
      templateUrl: "templates/header.html"
    };
  }
})();
