(function() {
  "use strict";
  angular.module("BookzSchoolApp").directive("pzFooter", pzFooter);
  pzFooter.$inject = [];

  function pzFooter() {
    return {
      restrict: "E",
      scope: {
        logo: "@",
        name: "@",
        email: "@",
        address: "@",
        contact: "@"
      },
      controller: [
        "$scope",
        "$rootScope",
        function($scope, $rootScope) {
          $scope.__getStr = $rootScope.__getStr;

          if (!$scope.logo && $rootScope.codeData) $scope.logo = $rootScope.codeData.seller.logo_link;
          if (!$scope.name && $rootScope.codeData) $scope.name = $rootScope.codeData.seller.name;
          if (!$scope.email && $rootScope.codeData) $scope.email = $rootScope.codeData.seller.email;
          if (!$scope.address && $rootScope.codeData) $scope.address = $rootScope.codeData.seller.address;
          if (!$scope.contact && $rootScope.codeData) $scope.contact = $rootScope.codeData.seller.contact;
        }
      ],
      link: function(scope, elem, attrs) {
        if (!scope.logo) scope.logo = attrs.logo;
        if (!scope.name) scope.name = attrs.name;
        if (!scope.email) scope.email = attrs.email;
        if (!scope.address) scope.address = attrs.address;
        if (!scope.contact) scope.contact = attrs.contact;
      },
      templateUrl: "templates/footer.html"
    };
  }
})();
