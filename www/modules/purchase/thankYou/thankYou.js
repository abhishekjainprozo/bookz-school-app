(function() {
  "use strict";
  angular.module("purchase").controller("thankYou", thankYou);
  thankYou.$inject = ["apis", "$state", "$ionicHistory", "$stateParams"];

  function thankYou(apis, $state, $ionicHistory, $stateParams) {
    console.log("thankYou");
    var vm = this; // jshint ignore:line
    vm.reqDetails = reqDetails;
    vm.newRequest = newRequest;
    vm.reqNb = $stateParams.reqNb;

    ////

    function reqDetails() {
      $state.go("reqDetails", {
        reqNb: vm.reqNb
      });
    }

    function newRequest() {
      $ionicHistory.clearHistory();
      $ionicHistory.clearCache();
      $state.go("schoolCode");
    }
  }
})();
