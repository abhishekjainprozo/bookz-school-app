(function() {
  "use strict";
  angular.module("purchase").controller("checkout", checkout);
  checkout.$inject = [
    "apis",
    "toast",
    "$rootScope",
    "$state",
    "$stateParams",
    "constants",
    "$timeout"
  ];

  function checkout(
    apis,
    toast,
    $rootScope,
    $state,
    $stateParams,
    constants,
    $timeout
  ) {
    console.log("checkout");
    var vm = this; // jshint ignore:line

    vm.paymentMethod = $rootScope.codeData.cod_allowed ? null : "ONLINE";
    vm.data = $stateParams.reqParams;

    vm.totalWithTax = $stateParams.totalWithTax;

    vm.logisticCharges =
      $rootScope.codeData.logistic_charges_bourne_by == 1 &&
      !$stateParams.reqParams.self_pick_up
        ? $rootScope.codeData.logistic_charges
        : 0;

    vm.name = "";
    if ($stateParams.reqParams.self_pick_up === 1)
      vm.name = $stateParams.reqParams.billing_address.name;
    else
      vm.name = $stateParams.reqParams.billing_address
        ? $stateParams.reqParams.billing_address.name
        : $stateParams.reqParams.shipping_address.name;

    vm.totalTax = $stateParams.totalTax;
    vm.internetFees = getGatewayCharges();

    vm.placeRequest = placeRequest;
    vm.getFinalTotal = getFinalTotal;
    vm.openTermsOfUse = openTermsOfUse;

    ////

    function placeRequest() {
      // alert("0. starting request");
      var params = $stateParams.reqParams;

      params.logistic_charges = vm.logisticCharges;

      if (vm.paymentMethod === "COD") {
        params.cod_charges =
          $rootScope.codeData.cod_charges_bourne_by == 1
            ? $rootScope.codeData.cod_charges
            : 0;
        params.payment_mode = 0;
        submitRequest(params);
      } else if (vm.paymentMethod === "ONLINE") {
        params.internet_handling_charges = $rootScope.codeData
          .gateway_charges_bourne_by
          ? vm.internetFees
          : 0;
        params.payment_mode = 1;
        doPayment(params);
      }
    }

    function submitRequest(params, successCallback) {
      console.log("params: ", params);
      // alert("4. submitting request");
      apis
        .placeRequest(params)
        .then(function(res) {
          if (successCallback && typeof successCallback === "function")
            successCallback();
          $state.go("reqDetails", {
            reqNb: res.headers().tracking_number
          });
        })
        .catch(function(err) {
          console.log("err: ", err);
          toast.show(
            "Something went wrong. Contact seller at " +
              $rootScope.codeData.seller.contact +
              " (err " +
              err.status +
              ")",
            "LONG"
          );
          sendOrderFailSms(
            vm.name,
            $rootScope.codeData.studentMobile,
            $rootScope.codeData.code
          );
        });
    }

    function doPayment(params) {
      // alert("1. starting razorpay");
      var finalCharge_in_rupees = getFinalTotal();
      var finalCharge_in_paisas = Math.floor(finalCharge_in_rupees * 100);

      var options = {
        description: $rootScope.codeData.class,
        // image: $rootScope.codeData.seller.https_logo_link,
        currency: "INR",
        name: $rootScope.codeData.seller.name,
        key: constants.razorpayKey,
        amount: finalCharge_in_paisas,
        prefill: {
          name: vm.name,
          email: $rootScope.codeData.studentEmail,
          contact: $rootScope.codeData.studentMobile
        },
        theme: {
          // color: "#F37254"
          color: "#007af6"
        }
      };
      var successCallback = function(payment_id) {
        // alert("2. razorpay success");
        params.payment_id = payment_id;
        submitRequest(params, function() {
          razorpayCapture(payment_id, finalCharge_in_paisas, params);
        });
      };

      var cancelCallback = function(error) {
        console.log("error", error);
        // $state.go("paymentFailed");
      };
      $timeout(function() {
        RazorpayCheckout.open(options, successCallback, cancelCallback);
      }, 1000);
    }

    function razorpayCapture(
      payment_id,
      finalCharge_in_paisas,
      placeOrderParams
    ) {
      apis
        .RazorpayCapture(payment_id, finalCharge_in_paisas)
        .then(function(res) {
          console.log("res: ", res);

          //3. send razorpay transfer request
          apis
            .RazorpayTransfer(
              payment_id,
              $rootScope.codeData.seller.razorpayAccountId,
              finalCharge_in_paisas
            )
            .then(function(res) {
              console.log("res: ", res);
            })
            .catch(function(err) {
              // toast.show("Something went wrong. Contact seller at " + $rootScope.codeData.seller.contact + " (err " + err.status + ")", "LONG");
            });
        })
        .catch(function(err) {
          sendCaptureFailSms(
            payment_id,
            vm.name,
            $rootScope.codeData && $rootScope.codeData.studentMobile
          );
          // toast.show("Something went wrong. Contact seller at " + $rootScope.codeData.seller.contact + " (err " + err.status + ")", "LONG");
        });
    }

    function sendCaptureFailSms(paymentId, buyerName, buyerContact) {
      var params = {
        type: "capture_fail",
        paymentId: paymentId,
        buyerName: buyerName,
        buyerContact: buyerContact
      };
      apis.sendSms(params);
    }

    function sendOrderFailSms(buyerName, buyerContact, schoolCode) {
      var params = {
        type: "order_fail",
        buyerName: buyerName,
        buyerContact: buyerContact,
        schoolCode: schoolCode
      };
      apis.sendSms(params);
    }

    function getGatewayCharges() {
      if (!$rootScope.codeData || !$rootScope.codeData.gatewayChargesRules)
        return 0;

      var totalChargeBeforeInternetFees, internetFees;

      if ($rootScope.codeData.logistic_charges_bourne_by == 1)
        totalChargeBeforeInternetFees = vm.totalWithTax + vm.logisticCharges;
      else totalChargeBeforeInternetFees = vm.totalWithTax;

      var rule = $rootScope.codeData.gatewayChargesRules.filter(function(rule) {
        return (
          totalChargeBeforeInternetFees >= rule.min_value &&
          totalChargeBeforeInternetFees <= rule.max_value
        );
      });

      if (!rule || !rule.length) internetFees = 0;
      else
        internetFees = (totalChargeBeforeInternetFees * rule[0].charges) / 100;

      internetFees = Math.round(internetFees * 100) / 100; // round to 2 decimal places
      return internetFees;
    }

    function getFinalTotal() {
      var extraCharge = 0;

      if (
        vm.paymentMethod === "COD" &&
        $rootScope.codeData &&
        $rootScope.codeData.cod_charges_bourne_by == 1
      )
        extraCharge += $rootScope.codeData.cod_charges;
      else if (
        vm.paymentMethod === "ONLINE" &&
        $rootScope.codeData &&
        $rootScope.codeData.gateway_charges_bourne_by == 1
      )
        extraCharge += vm.internetFees;

      if (
        $rootScope.codeData &&
        $rootScope.codeData.logistic_charges_bourne_by == 1
      )
        extraCharge += vm.logisticCharges;

      return vm.totalWithTax + extraCharge;
    }

    function openTermsOfUse(e) {
      if (window.cordova && window.cordova.InAppBrowser)
        cordova.InAppBrowser.open(
          "http://prozo.com/terms-of-use.php",
          "_self",
          "location=yes"
        );
    }

    //
  }
})();
