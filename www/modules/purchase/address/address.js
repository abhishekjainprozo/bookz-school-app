(function() {
  "use strict";
  angular.module("purchase").controller("address", address);
  address.$inject = ["apis", "toast", "$stateParams", "$rootScope", "$state"];

  function address(apis, toast, $stateParams, $rootScope, $state) {
    // xxx
    // $rootScope.codeData = {
    //   id: 2,
    //   code: 270210,
    //   school: {
    //     id: 2,
    //     name: "Venkateshwar Global School",
    //     address: "Sector 13, Rohini,Delhi, 110085",
    //     logo: "vschool.png",
    //     type: 0,
    //     is_deleted: 0,
    //     status: 1,
    //     created_date: 1515399106000,
    //     logo_link:
    //       "http://admin.prozo.com/backend/web/uploads/schoolapp/logo/vschool.png"
    //   },
    //   seller: {
    //     id: 3,
    //     name: "Janta Book Depot",
    //     address:
    //       "D-21, South Extension Part 1\r\nNear Dena Bank\r\nNew Delhi, 110049",
    //     logo: "jbd.png",
    //     contact: "01124621355 ",
    //     razorpayAccountId: "acc_C6spi9yl6JTuwa",
    //     email: "jbdbookshop@gmail.com",
    //     type: 0,
    //     is_deleted: 0,
    //     status: 1,
    //     https_logo_link:
    //       "https://s3.ap-south-1.amazonaws.com/school.app/FMVRUc0.png",
    //     logo_link:
    //       "http://admin.prozo.com/backend/web/uploads/schoolapp/logo/jbd.png"
    //   },
    //   classes: [
    //     "Pre School",
    //     "Pre Primary",
    //     "Class 1",
    //     "Class 2",
    //     "Class 3",
    //     "Class 4",
    //     "Class 5",
    //     "Class 6 (French)",
    //     "Class 6 (German)",
    //     "Class 6 (Sanskrit)",
    //     "Class 7 (German)",
    //     "Class 7 (Sanskrit)",
    //     "Class 7 (French)",
    //     "Class 8 (German)",
    //     "Class 8 (Sanskrit)",
    //     "Class 8 (French)",
    //     "Class 9",
    //     "Class 11"
    //   ],
    //   cod_allowed: 0,
    //   payment_gateway: "RP",
    //   logistic_charges_bourne_by: "1",
    //   logistic_charges: 0,
    //   cod_charges_bourne_by: 1,
    //   cod_charges: 0,
    //   gateway_charges_bourne_by: 1,
    //   created_date: 1515399175000,
    //   self_pick_up_allowed: 1,
    //   gatewayChargesRules: [
    //     {
    //       id: 1,
    //       code: "RP",
    //       charges: 0.708,
    //       max_value: 2000,
    //       min_value: 1
    //     },
    //     {
    //       id: 2,
    //       code: "RP",
    //       charges: 2.596,
    //       max_value: 1000000,
    //       min_value: 2001
    //     }
    //   ],
    //   studentID: 4682,
    //   class: "Pre School",
    //   studentEmail: "01abhishekjain@gmail.com",
    //   studentMobile: 8700147078
    // };
    // xxx
    console.log("address");
    var vm = this; // jshint ignore:line
    vm.loadCities = loadCities;
    vm.goToCheckout = goToCheckout;
    vm.selfPickUpAllowed = $rootScope.codeData.self_pick_up_allowed;
    activate();

    ////

    function activate() {
      apis
        .states()
        .then(function(res) {
          if (res.data && res.data.splice && res.data.length) {
            vm.states = res.data;
          }
        })
        .catch(function(err) {
          toast.show(
            "Something went wrong. Contact seller at " +
              $rootScope.codeData.seller.contact +
              " (err " +
              err.status +
              ")",
            "LONG"
          );
        });
    }

    function loadCities(stateID, addressObject) {
      apis
        .cities(stateID)
        .then(function(res) {
          if (res.data && res.data.splice && res.data.length) {
            addressObject.cities = res.data;
          }
        })
        .catch(function(err) {
          toast.show(
            "Something went wrong. Contact seller at " +
              $rootScope.codeData.seller.contact +
              " (err " +
              err.status +
              ")",
            "LONG"
          );
        });
    }

    function goToCheckout() {
      var params = {
        student: {
          id: $rootScope.codeData.studentID,
          class_number: $rootScope.codeData.class
        },
        seller: {
          id: $rootScope.codeData.seller.id
        },
        school: {
          id: $rootScope.codeData.school.id
        },
        payment_mode: 0,
        payment_status: 1,
        orderProduct: $stateParams.selectedProds,
        self_pick_up: vm.form.selfPickUp ? 1 : 0
      };

      if (vm.form.selfPickUp || vm.addBillingAddr)
        params.billing_address = {
          type: 1,
          name: vm.form.billing.firstName + " " + vm.form.billing.lastName,
          address: vm.form.billing.line1,
          pincode: vm.form.billing.pincode,
          city: {
            id: vm.form.billing.city
          },
          state: vm.form.billing.state,
          landmark: vm.form.billing.landmark
        };

      if (!vm.form.selfPickUp)
        params.shipping_address = {
          type: 0,
          name: vm.form.shipping.firstName + " " + vm.form.shipping.lastName,
          address: vm.form.shipping.line1,
          pincode: vm.form.shipping.pincode,
          city: {
            id: vm.form.shipping.city
          },
          state: vm.form.shipping.state,
          landmark: vm.form.shipping.landmark
        };

      console.log("params: ", params);
      $state.go("checkout", {
        reqParams: params,
        totalTax: $stateParams.totalTax,
        totalWithTax: $stateParams.totalWithTax
      });
    }
  }
})();
