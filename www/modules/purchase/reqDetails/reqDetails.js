(function() {
  "use strict";
  angular.module("purchase").controller("reqDetails", reqDetails);
  reqDetails.$inject = [
    "apis",
    "toast",
    "$ionicPopup",
    "resolvedData",
    "$ionicHistory",
    "$rootScope",
    "$scope",
    "$ionicScrollDelegate",
    "$state"
  ];

  function reqDetails(
    apis,
    toast,
    $ionicPopup,
    resolvedData,
    $ionicHistory,
    $rootScope,
    $scope,
    $ionicScrollDelegate,
    $state
  ) {
    $scope.$on("$ionicView.enter", function() {
      $ionicHistory.currentView().canSwipeBack =
        $ionicHistory.backView() &&
        $ionicHistory.backView().stateName !== "address";
    });

    //xxx
    // resolvedData = resolvedData || {"id":142,"student":{"id":306,"first_name":"lkj","mobile":"9871818390","last_name":"lkjs","email":"dfv@sfv.com","class_number":"Class 1","handler":{},"hibernateLazyInitializer":{}},"shipping_address":{"id":150,"type":0,"contact":"9898989898","address":"nirman vihar","landmark":"landmark","pincode":"110092","city":{"id":591,"name":"Delhi","state":{"id":4,"name":"Delhi","handler":{},"hibernateLazyInitializer":{}},"handler":{},"hibernateLazyInitializer":{}},"handler":{},"hibernateLazyInitializer":{}},"tracking_number":"BK001420002","seller":{"id":2,"name":"Arora Stationers","logo":"arora.png","created_date":1518704866000,"logo_link":"http://admin.prozo.com/backend/web/uploads/schoolapp/logo/arora.png","handler":{},"hibernateLazyInitializer":{}},"school":{"id":2,"name":"Venkateshwar Global School","address":"Sector 18A, Dwarka, New Delhi","logo":"vschool.png","type":0,"is_deleted":0,"status":1,"created_date":1518704924000,"logo_link":"http://admin.prozo.com/backend/web/uploads/schoolapp/logo/vschool.png","handler":{},"hibernateLazyInitializer":{}},"value":7790.0,"invoice_value":7790,"discount":0.0,"tax_amount":0.0,"payment_mode":0,"payment_status":1,"is_deleted":0,"is_cancel":0,"created_date":"22-02-2018 22:24","orderProduct":[{"id":281,"product":{"id":13,"code":102030,"class_number":"Class 7","title":"Books","description":"English - Course, Supplementary and Literature Reader\r\nHindi - Course and Vyakran\r\nMathematics\r\nGeneral Knowledge\r\nScience\r\nScoial Science\r\nArt\r\nComputer\r\nGerman/French/Sanskrit","image":"bundle.png","mrp":5600.0,"discount":0.0,"discount_percent":0.0,"tax_1":0.0,"tax_1_amount":0.0,"tax_2":0.0,"tax_2_amount":0.0,"selling_price":5600.0,"tax_3":0.0,"tax_3_amount":0.0,"created_date":1515568724000,"image_link":"http://admin.prozo.com/backend/web/uploads/schoolapp/products/bundle.png"},"mrp":5600.0,"discount_percent":0.0,"discount":0.0,"tax_1":0.0,"tax_1_amount":0.0,"tax_2":0.0,"tax_2_amount":0.0,"tax_3":0.0,"tax_3_amount":0.0,"selling_price":5600.0},{"id":282,"product":{"id":14,"code":102030,"class_number":"Class 7","title":"School Bag","image":"bag.png","mrp":590.0,"discount":0.0,"discount_percent":0.0,"tax_1":0.0,"tax_1_amount":0.0,"tax_2":0.0,"tax_2_amount":0.0,"selling_price":590.0,"tax_3":0.0,"tax_3_amount":0.0,"created_date":1515568724000,"image_link":"http://admin.prozo.com/backend/web/uploads/schoolapp/products/bag.png"},"mrp":590.0,"discount_percent":0.0,"discount":0.0,"tax_1":0.0,"tax_1_amount":0.0,"tax_2":0.0,"tax_2_amount":0.0,"tax_3":0.0,"tax_3_amount":0.0,"selling_price":590.0},{"id":283,"product":{"id":15,"code":102030,"class_number":"Class 7","title":"Subject Enrichment Activity Charges","image":"misc.png","mrp":400.0,"discount":0.0,"discount_percent":0.0,"tax_1":0.0,"tax_1_amount":0.0,"tax_2":0.0,"tax_2_amount":0.0,"selling_price":400.0,"tax_3":0.0,"tax_3_amount":0.0,"created_date":1515568724000,"image_link":"http://admin.prozo.com/backend/web/uploads/schoolapp/products/misc.png"},"mrp":400.0,"discount_percent":0.0,"discount":0.0,"tax_1":0.0,"tax_1_amount":0.0,"tax_2":0.0,"tax_2_amount":0.0,"tax_3":0.0,"tax_3_amount":0.0,"selling_price":400.0},{"id":284,"product":{"id":16,"code":102030,"class_number":"Class 7","title":"Art & Craft Charges","image":"art.png","mrp":1200.0,"discount":0.0,"discount_percent":0.0,"tax_1":0.0,"tax_1_amount":0.0,"tax_2":0.0,"tax_2_amount":0.0,"selling_price":1200.0,"tax_3":0.0,"tax_3_amount":0.0,"created_date":1515568724000,"image_link":"http://admin.prozo.com/backend/web/uploads/schoolapp/products/art.png"},"mrp":1200.0,"discount_percent":0.0,"discount":0.0,"tax_1":0.0,"tax_1_amount":0.0,"tax_2":0.0,"tax_2_amount":0.0,"tax_3":0.0,"tax_3_amount":0.0,"selling_price":1200.0}]}

    // $rootScope.codeData = $rootScope.codeData || {
    //   id: 1,
    //   code: 657832,
    //   school: {
    //     id: 1,
    //     name: "DPS Noida",
    //     address: "Sector 132, Noida Expressway, Noida",
    //     logo: "dpsj.png",
    //     type: 0,
    //     is_deleted: 0,
    //     status: 1,
    //     created_date: 1515399106000,
    //     logo_link: "http://staging.prozo.com/backend/web/uploads/schoolapp/logo/dpsj.png"
    //   },
    //   seller: {
    //     id: 1,
    //     name: "Malik Distributors",
    //     address: "Daryaganj, New Delhi",
    //     logo: "malik.png",
    //     email: "test@gmail.com",
    //     type: 0,
    //     is_deleted: 0,
    //     status: 1,
    //     created_date: 1515399153000,
    //     logo_link: "http://staging.prozo.com/backend/web/uploads/schoolapp/logo/malik.png"
    //   },
    //   classes: ["1", "2", "3", "4", "5", "6", "7", "8"],
    //   cod_allowed: 1,
    //   logistic_charges_bourne_by: "0",
    //   created_date: 1515399175000,
    //   studentID: "177",
    //   class: "8"
    // };
    //xxx

    console.log("reqDetails");
    var vm = this; // jshint ignore:line
    vm.cancelOrder = cancelOrder;
    vm.data = massageData(resolvedData);
    vm.back = back;
    vm.openInvoice = openInvoice;
    vm.hideCancel =
      $ionicHistory.backView() &&
      $ionicHistory.backView().stateName === "address";
    vm.shareApp = shareApp;

    ////

    function cancelOrder() {
      // A confirm dialog

      var confirmPopup = $ionicPopup.confirm({
        cssClass: "req-popup",
        title: "Cancel order",
        template: "Are you sure you want to cancel the order?"
      });

      confirmPopup.then(function(res) {
        if (res) {
          apis
            .cancelOrder(vm.data.id)
            .then(function(res) {
              vm.data.is_cancel = 1;
              // on iOS, need to manually scroll to the top
              // since after cancelling, the page height will be much less than before,
              // and as the cancel button was at the bottom of the screen
              $ionicScrollDelegate.scrollTop();

              if (vm.data.payment_mode == 1 && vm.data.payment_id)
                apis
                  .RazorpayRefund(vm.data.payment_id)
                  .then(function(res) {
                    console.log("res: ", res);
                  })
                  .catch(function(err) {
                    toast.show(
                      "Something went wrong. Contact seller at " +
                        $rootScope.codeData.seller.contact +
                        " (err " +
                        err.status +
                        ")",
                      "LONG"
                    );
                  });
            })
            .catch(function(err) {
              if (err.status === 404) toast.show("No records found", "MEDIUM");
              else
                toast.show(
                  "Something went wrong. Contact seller at " +
                    $rootScope.codeData.seller.contact +
                    " (err " +
                    err.status +
                    ")",
                  "LONG"
                );
            });
        }
      });
    }

    function back() {
      $ionicHistory.clearHistory();
      $ionicHistory.clearCache();
      $state.go("schoolCode");
    }

    function massageData(data) {
      if (!data.orderProduct) return data;

      // create products array
      data.orderProduct.forEach(function(prod) {
        prod.product.description =
          prod.product &&
          prod.product.description &&
          prod.product.description.split("\r\n");
        if (prod.product.description) prod.product.description.pop(); // remove last item as it is always empty
      });

      // set the payment mode string
      if (data.paymentDetails && data.paymentDetails.method) {
        if (data.paymentDetails.method.toLowerCase() === "netbanking") {
          if (data.paymentDetails.bank)
            data.payment_mode_str =
              "Netbanking (" + data.paymentDetails.bank + ")";
          else data.payment_mode_str = "Netbanking";
        } else if (data.paymentDetails.method.toLowerCase() === "wallet") {
          if (data.paymentDetails.wallet)
            data.payment_mode_str =
              "Wallet (" + data.paymentDetails.wallet + ")";
          else data.payment_mode_str = "Wallet";
        } else if (data.paymentDetails.method.toLowerCase() === "card")
          data.payment_mode_str = "Card";
        else if (data.paymentDetails.method.toLowerCase() === "upi")
          data.payment_mode_str = "UPI";
      }

      // set the refund status
      if (
        data.payment_mode == 1 &&
        data.payment_id &&
        data.paymentDetails &&
        data.paymentDetails.status === "refunded"
      ) {
        data.refund_status_str = "Refunded";
        if (data.paymentDetails.refund_status)
          data.refund_status_str +=
            " (" + data.paymentDetails.refund_status + ")";
      } else data.refund_status_str = "Not available";

      return data;
    }

    function openInvoice(fullPath) {
      if (window.cordova && window.cordova.InAppBrowser)
        cordova.InAppBrowser.open(fullPath, "_system");
    }

    function shareApp() {
      var message =
        "Hi! \nNow, you can buy your children's school books and other study material bundle at the comfort of your home using *The School Bookz App*. Try 270210 code for VGS, Rohini school. Contact 9873112459 for creating similar mobile app for your school. Do refer us to your school if you find this app useful. \nRegards,\nProzo\nDownload app now:http://onelink.to/v4r4b5";
      if (window.plugins && window.plugins.socialsharing)
        window.plugins.socialsharing.share(message);
    }

    //
  }
})();
