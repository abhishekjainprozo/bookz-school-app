(function() {
  "use strict";
  angular.module("purchase").controller("packages", packages);
  packages.$inject = ["apis", "toast", "$rootScope", "$state", "$ionicHistory"];

  function packages(apis, toast, $rootScope, $state, $ionicHistory) {
    console.log("packages");
    var vm = this; // jshint ignore:line
    vm.calcTotal = calcTotal;
    vm.placeRequest = placeRequest;
    vm.back = back;
    activate();

    ////

    function activate() {
      // xxx
      // vm.packages = [
      //   {
      //     id: 1,
      //     code: 657832,
      //     class_number: "8",
      //     title: "Books",
      //     description: "Physics\r\nChemistry\r\nBiology\r\nMathematcis",
      //     image: "123.png",
      //     mrp: 2000.0,
      //     discount: 10.0,
      //     selling_price: 1800.0,
      //     summary: "Set of 3 books",
      //     created_date: 1515492230000,
      //     image_link: "http://localhost/prozob2b/backend/web/uploads/schoolapp/products/123.png"
      //   },
      //   {
      //     id: 2,
      //     code: 657832,
      //     class_number: "8",
      //     title: "Books",
      //     description: "1212\r\n12121\r\n342324",
      //     image: "345.png",
      //     mrp: 1000.0,
      //     discount: 0.0,
      //     tax_1: 5.0,
      //     tax_1_amount: 50.0,
      //     tax_2: 5.0,
      //     tax_2_amount: 50.0,
      //     selling_price: 1100.0,
      //     tax_3: 10.0,
      //     tax_3_amount: 100.0,
      //     created_date: 1515568678000,
      //     image_link: "http://localhost/prozob2b/backend/web/uploads/schoolapp/products/345.png"
      //   },
      //   {
      //     id: 3,
      //     code: 657832,
      //     class_number: "8",
      //     title: "Stationary",
      //     description: "Diary\r\nPen Box",
      //     mrp: 200.0,
      //     discount: 10.0,
      //     tax_1: 5.0,
      //     tax_1_amount: 9.0,
      //     tax_2: 5.0,
      //     tax_2_amount: 9.0,
      //     selling_price: 198.0,
      //     tax_3: 10.0,
      //     tax_3_amount: 18.0,
      //     created_date: 1515568724000
      //   }
      // ];

      // vm.packages.forEach(function(prod) {
      //   prod.selected = true;
      //   prod.sp_withoutTax = prod.mrp - prod.mrp * prod.discount / 100;
      //   prod.description = prod.description && prod.description.split("\r\n");
      // });

      // calcTotal();
      // xxx

      vm.packages = setPackages();
    }

    function setPackages() {
      var code = $rootScope.codeData.code;
      var studentID = $rootScope.codeData.studentID;
      apis
        .packages(code, studentID)
        .then(function(res) {
          console.log("res: ", res);

          vm.packages = res.data;

          vm.packages.forEach(function(prod) {
            prod.selected = true;
            prod.sp_withoutTax = prod.mrp - prod.discount;
            prod.description =
              prod.description && prod.description.split("\r\n");
            if (prod.description) prod.description.pop(); // remove last item as it is always empty
          });

          calcTotal();
        })
        .catch(function(err) {
          if (err.status === 404) toast.show("No records found", "MEDIUM");
          else
            toast.show(
              "Something went wrong. Contact seller at " +
                $rootScope.codeData.seller.contact +
                " (err " +
                err.status +
                ")",
              "LONG"
            );
        });
    }

    function calcTotal(prodIdx) {
      if (
        prodIdx !== undefined &&
        toast &&
        vm.packages &&
        vm.packages.length &&
        vm.packages[prodIdx] &&
        vm.packages[prodIdx].is_mandatory === 1
      ) {
        vm.packages[prodIdx].selected = true;
        return toast.show("Sorry, you can’t deselect this cart item", "MEDIUM");
      }
      if (
        prodIdx !== undefined &&
        toast &&
        vm.packages &&
        vm.packages.length &&
        vm.packages[prodIdx] &&
        vm.packages[prodIdx].is_mandatory === 0
      ) {
        if (vm.packages[prodIdx].selected)
          toast.show(vm.packages[prodIdx].title + ": selected", "MEDIUM");
        else if (!vm.packages[prodIdx].selected)
          toast.show(vm.packages[prodIdx].title + ": de-selected", "MEDIUM");
      }

      var totalWithoutTax = 0;
      var totalTax = 0;
      var totalWithTax = 0;

      vm.packages.forEach(function(prod) {
        if (prod.selected) {
          totalWithoutTax += parseInt(prod.sp_withoutTax);
          totalWithTax += parseInt(prod.selling_price);
        }
      });
      totalTax = totalWithTax - totalWithoutTax;

      vm.totalWithoutTax = totalWithoutTax;
      vm.totalTax = totalTax;
      vm.totalWithTax = totalWithTax;
    }

    function placeRequest() {
      var selectedProds = vm.packages
        .filter(function(prod) {
          return prod.selected;
        })
        .map(function(prod) {
          return { product: { id: prod.id } };
        });
      console.log("selectedProds: ", selectedProds);
      $state.go("address", {
        selectedProds: selectedProds,
        totalTax: vm.totalTax,
        totalWithTax: vm.totalWithTax
      });
    }

    function back() {
      $ionicHistory.goBack(-2);
    }
  }
})();
