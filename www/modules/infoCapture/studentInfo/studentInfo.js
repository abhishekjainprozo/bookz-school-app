(function() {
  "use strict";
  angular.module("infoCapture").controller("studentInfo", studentInfo);
  studentInfo.$inject = ["$state", "apis", "toast", "$rootScope"];

  function studentInfo($state, apis, toast, $rootScope) {
    console.log("studentInfo");
    var vm = this; // jshint ignore:line
    vm.requestOTP = requestOTP;

    function requestOTP() {
      apis
        .requestOTP(vm.firstName, vm.lastName, vm.mobile, vm.email, vm.class)
        .then(function(res) {
          $rootScope.codeData.studentID = res.headers().id;
          // $rootScope.codeData.studentID = 4707; // xxx
          $rootScope.codeData.class = vm.class;
          $rootScope.codeData.studentName = vm.firstName + " " + vm.lastName;
          $rootScope.codeData.studentEmail = vm.email;
          $rootScope.codeData.studentMobile = vm.mobile;
          $state.go("otpVerify", { mobile: vm.mobile });
        })
        .catch(function(err) {
          toast.show(
            "Something went wrong. Contact seller at " +
              $rootScope.codeData.seller.contact +
              " (err " +
              err.status +
              ")",
            "LONG"
          );
        });
    }
  }
})();
