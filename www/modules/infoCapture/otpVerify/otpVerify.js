(function() {
  "use strict";
  angular.module("infoCapture").controller("otpVerify", otpVerify);
  otpVerify.$inject = [
    "$stateParams",
    "apis",
    "$interval",
    "$ionicPopup",
    "$scope",
    "$rootScope",
    "toast",
    "$state"
  ];

  function otpVerify(
    $stateParams,
    apis,
    $interval,
    $ionicPopup,
    $scope,
    $rootScope,
    toast,
    $state
  ) {
    var vm = this; // jshint ignore:line

    vm.verifyOTP = verifyOTP;
    vm.resend = resend;

    vm.mobile = $stateParams.mobile;
    vm.studentID = $stateParams.studentID;

    var WAIT_TIME = 30;
    vm.MAX_RESENDS = 2;
    vm.resends = 0;

    resetTimer();

    /////

    function resetTimer() {
      vm.sec = WAIT_TIME;
      var timer = $interval(function() {
        vm.sec--;
        if (vm.sec === 0) $interval.cancel(timer);
      }, 1000);
    }

    function verifyOTP() {
      apis
        .verifyOTP(
          vm.mobile,
          vm.otp,
          $rootScope.codeData.studentID,
          $rootScope.codeData.code
        )
        .then(function(res) {
          showPopup(res.headers().message);
        })
        .catch(function(err) {
          if (err.status === 409) toast.show("Invalid OTP", "MEDIUM");
          else
            toast.show(
              "Something went wrong. Contact seller at " +
                $rootScope.codeData.seller.contact +
                " (err " +
                err.status +
                ")",
              "LONG"
            );
        });
    }

    function showPopup(message) {
      var scope = $scope.$new();
      scope.msg = message;
      scope.close = function() {
        p.close();
      };

      var p = $ionicPopup.show({
        cssClass: "pzPopup",
        templateUrl: "templates/loginSuccessful.html",
        scope: scope
      });

      p.then(function() {
        $state.go("packages");
      });
    }

    function resend() {
      apis
        .resendOtp(vm.mobile)
        .then(function() {
          resetTimer();
          vm.resends++;
        })
        .catch(function(err) {
          toast.show(
            "Something went wrong. Contact seller at " +
              $rootScope.codeData.seller.contact +
              " (err " +
              err.status +
              ")",
            "LONG"
          );
        });
    }
  }
})();
