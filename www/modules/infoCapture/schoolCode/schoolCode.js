(function() {
  "use strict";
  angular.module("infoCapture").controller("schoolCode", schoolCode);
  schoolCode.$inject = ["$state", "apis", "toast", "$rootScope", "$scope", "$ionicPopup"];

  function schoolCode($state, apis, toast, $rootScope, $scope, $ionicPopup) {
    var vm = this; // jshint ignore:line
    vm.checkCode = checkCode;
    vm.viewPreviousOrder = viewPreviousOrder;
    vm.shareApp = shareApp;

    function checkCode() {
      apis
        .schoolCode(vm.code)
        .then(function(res) {
          if (res.status === 200) {
            res.data.classes = res.data.classes.split(",");
            $rootScope.codeData = res.data;
            $state.go("studentInfo");
          } else toast.show("Something went wrong. Please try again later. (err " + err.status + ")", "LONG");
        })
        .catch(function(err) {
          if (err.status === 404) toast.show("Incorrect code. Please try again.", "MEDIUM");
          else toast.show("Something went wrong. Please try again later. (err " + err.status + ")", "LONG");
        });
    }

    function viewPreviousOrder() {
      // An elaborate, custom popup
      var scope = $scope.$new();
      scope.data = {};
      var myPopup = $ionicPopup.show({
        template:
          '<label for="ref">Enter reference number</label>' +
          '<input type="text" name="ref" ng-model="data.reqNb">' +
          '<label for="mob">Enter mobile number</label>' +
          '<input type="number" name="mob" ng-model="data.mobNb">' +
          '<span ng-if="mobErr">Please enter a 10 digit number</span>',
        title: "Track order",
        cssClass: "req-popup",
        // subTitle: "Its",
        scope: scope,
        buttons: [
          {
            text: "Cancel",
            onTap: function() {
              myPopup.close();
            }
          },
          {
            text: "View",
            type: "button-positive",
            cls: "req-popup__btn",
            onTap: function(e) {
              if (!scope.data.reqNb || !scope.data.mobNb || scope.data.mobNb.toString().length !== 10) {
                if (!scope.data.mobNb || scope.data.mobNb.toString().length !== 10) scope.mobErr = true;
                e.preventDefault();
              } else {
                myPopup.close();
                return $state.go("reqDetails", {
                  reqNb: scope.data.reqNb,
                  mobNb: scope.data.mobNb
                });
              }
            }
          }
        ]
      });

      myPopup.then(function(res) {
        console.log("Tapped!", res);
      });
    }

    function shareApp() {
      var message =
        "Hi! \nNow, you can buy your children's school books and other study material bundle at the comfort of your home using *The School Bookz App*. Try 270210 code for VGS, Rohini school. Contact 9873112459 for creating similar mobile app for your school. Do refer us to your school if you find this app useful. \nRegards,\nProzo\nDownload app now:http://onelink.to/v4r4b5";
      if (window.plugins && window.plugins.socialsharing) window.plugins.socialsharing.share(message);
    }

    //
  }
})();
