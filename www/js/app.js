"use strict";
(function() {
  angular.module("infoCapture", []);
  angular.module("purchase", []);

  angular
    .module("BookzSchoolApp", ["ionic", "infoCapture", "purchase"])
    .config(config)
    .constant("constants", {
      version: "1.6.2",
      // STAGING ------------------------------------------------------

      // razorpayKey: "rzp_test_yyDIyiz1d0sj5j",
      // razorpaySecret: "n4NnIfXQaPRyDQ2LnrBMiKXt",
      // url: "http://staging.prozo.com:8092/api/",
      // url: "http://192.168.1.7:8092/api/",
      // url: "http://192.168.1.21:8092/api/",
      // auth1: "john",
      // auth2: "password"

      // STAGING ------------------------------------------------------

      // LIVE !!! ------------------------------------------------------

      // razorpayKey: "rzp_test_yyDIyiz1d0sj5j",
      // razorpaySecret: "n4NnIfXQaPRyDQ2LnrBMiKXt",
      razorpayKey: "rzp_live_PHGkWtKirEKQuI",
      razorpaySecret: "ycW5S2KTo9q915VWIQNLpnGu",
      url: "http://admin.prozo.com:8092/api/",
      auth1: "prozo",
      auth2: "721b66f5-6842-48b3-accf-ed6215de7559"

      // LIVE !!! ----------------------------------------------------
    })
    .run(run);

  //////////

  function config(
    $stateProvider,
    $urlRouterProvider,
    $httpProvider,
    constants
  ) {
    // $httpProvider.interceptors.push("LoadingInterceptor");

    var base64encoded = btoa(constants.auth1 + ":" + constants.auth2);
    $httpProvider.defaults.headers.common.Authorization =
      "Basic " + base64encoded;

    $httpProvider.interceptors.push("interceptor");

    $stateProvider
      .state("schoolCode", {
        url: "/schoolCode",
        templateUrl: "modules/infoCapture/schoolCode/schoolCode.html",
        controller: "schoolCode as vm"
      })
      .state("studentInfo", {
        url: "/studentInfo",
        templateUrl: "modules/infoCapture/studentInfo/studentInfo.html",
        controller: "studentInfo as info"
      })
      .state("otpVerify", {
        url: "/otpVerify",
        params: {
          mobile: null
        },
        templateUrl: "modules/infoCapture/otpVerify/otpVerify.html",
        controller: "otpVerify as otp"
      })
      .state("packages", {
        url: "/packages",
        templateUrl: "modules/purchase/packages/packages.html",
        controller: "packages as packages"
      })
      .state("address", {
        url: "/address",
        params: {
          selectedProds: null,
          totalWithTax: null,
          totalTax: null
        },
        templateUrl: "modules/purchase/address/address.html",
        controller: "address as vm"
      })
      .state("checkout", {
        url: "/checkout",
        params: {
          reqParams: null,
          totalWithTax: null,
          totalTax: null
        },
        templateUrl: "modules/purchase/checkout/checkout.html",
        controller: "checkout as vm"
      })
      // .state("thankYou", {
      //   url: "/thankYou?reqNb",
      //   templateUrl: "modules/purchase/thankYou/thankYou.html",
      //   controller: "thankYou as vm"
      // })
      .state("reqDetails", {
        url: "/reqDetails?reqNb?mobNb",
        templateUrl: "modules/purchase/reqDetails/reqDetails.html",
        controller: "reqDetails as vm",
        resolve: {
          resolvedData: resolvedData
        }
      });
    $urlRouterProvider.otherwise("schoolCode");
  }

  resolvedData.$inject = ["$stateParams", "apis", "toast", "$q"];
  function resolvedData($stateParams, apis, toast, $q) {
    var defer = $q.defer();

    var reqNb = $stateParams.reqNb;
    var mobNb = $stateParams.mobNb;
    apis
      .reqDetails(reqNb, mobNb)
      .then(function(res) {
        defer.resolve(res.data);
      })
      .catch(function(err) {
        if (err.status === 404) toast.show("No records found", "MEDIUM");
        else
          toast.show(
            "Something went wrong. Please try again later. (err " +
              err.status +
              ")",
            "LONG"
          );
        defer.reject(err);
      });

    return defer.promise;
  }

  function run(
    $ionicPlatform,
    $rootScope,
    l10n,
    $state,
    $timeout,
    toast,
    $ionicHistory,
    apis,
    $ionicPopup,
    constants
  ) {
    $ionicPlatform.ready(function() {
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);

        // Don't remove this line unless you know what you are doing. It stops the viewport
        // from snapping when text inputs are focused. Ionic handles this internally for
        // a much nicer keyboard experience.
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if (window.StatusBar) {
        window.StatusBar.styleDefault();
        window.StatusBar.overlaysWebView(false);
      }
    });

    l10n.set();
    $rootScope.__getStr = l10n.get;

    var BackButton = 0;
    var pages_backBtnDisabled = ["schoolCode", "packages"];
    var pages_exitApp = ["schoolCode"];

    $ionicPlatform.registerBackButtonAction(function(event) {
      // back button will be disabled or not on the request details page depending on
      // whether the page was opened right after placing the order,
      // or if it is opened to track a previous order
      if (
        $state.$current.name === "reqDetails" &&
        $ionicHistory.backView().stateName === "checkout"
      ) {
        pages_backBtnDisabled.push("reqDetails");
        pages_exitApp.push("reqDetails");
      }

      if (pages_backBtnDisabled.indexOf($state.$current.name) === -1)
        $ionicHistory.goBack();
      else if (pages_exitApp.indexOf($state.$current.name) !== -1) {
        if (BackButton === 0) {
          BackButton++;
          toast.show("Please press BACK again to exit", "MEDIUM");
          $timeout(function() {
            BackButton = 0;
          }, 3000);
        } else if (navigator.app) navigator.app.exitApp();
      }
    }, 100);

    // close keyboard on form submits
    document.addEventListener("submit", function() {
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.close();
      }
    });

    $rootScope.$on("$stateChangeSuccess", function(
      event,
      toState,
      toParams,
      fromState,
      fromParams
    ) {
      // auto focus on the first input element on screen
      $timeout(function() {
        var input = document.querySelector('.pane[nav-view="active"] input');
        if (input && input.focus) input.focus();
      }, 700);
    });

    // check if app needs updating
    var currVersion = constants.version;
    apis
      .getAppVersion(currVersion)
      .then(function(res) {
        console.log("res: ", res);
        var latestVersion = res.data.latestVersion;
        var isDifferent = isVerDiff(currVersion, latestVersion);
        if (!isDifferent) return;

        (function applyBkBtnOverride() {
          $ionicPlatform.registerBackButtonAction(
            function(e) {
              return; // do nothing
            },
            401,
            "popup-override"
          );
        })();

        var scope = $rootScope.$new();
        var OS = (ionic && ionic.Platform.platform()) || "android";
        scope.dataLink =
          OS === "android" ? res.data.playStoreLink : res.data.appStoreLink;
        scope.updateMandatory = res.data.isCurrentRedundant;
        // scope.updateMandatory = false;
        scope.currentVer = currVersion;
        scope.serverVer = latestVersion;
        var updatePopup = $ionicPopup.show({
          cssClass: "updateApp",
          scope: scope,
          templateUrl: "templates/update-available.html"
        });

        scope.closePopup = function() {
          updatePopup.close();
        };
      })
      .catch(function(err) {
        console.log("err: ", err);
      });

    function isVerDiff(current, latest) {
      current = parseInt(
        current
          .toString()
          .split(".")
          .join("")
      );
      latest = parseInt(
        latest
          .toString()
          .split(".")
          .join("")
      );

      return current !== latest;
    }

    // show the environment on top left if not on prod
    if (constants.url !== "http://admin.prozo.com:8092/api/") {
      var env = document.createElement("span");
      env.innerHTML = constants.url;
      env.setAttribute(
        "style",
        "position: absolute; top: 0; left: 0; z-index: 9999; font-size: xx-small; background: #ff000078; color: white; font-weight: bold;"
      );
      document.body.append(env);
    }

    //
  }
})();
